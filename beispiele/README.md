# Beispiele aus der Vorlesung

Dateien mit kommentierten Beispielen aus der Vorlesung sowie weitergehenden
Beispielen und Anmerkungen.
Viele der Beispiele aus der Vorlesung wandern nach den Vorlesungen in aufbereiteter
Form hierher, ergänzt um Kommentare sowie zusätzliche Erklärungen/Erweiterungen.

Allerdings kann die Aufbereitung ggf. etwas dauern.
Die "rohen" Beispiele aus den Vorlesungen werden jeweils direkt nach der Vorlesung
zum Ordner "code_vorlesung" hinzugefügt.
