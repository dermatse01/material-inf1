package main

import (
	"fmt"
)

func main() {
	// Direkte Berechnung der Fakultät in der Ausgabe.
	//fmt.Println(1 * 2 * 3 * 4 * 5)

	// Variante mit Zwischenergebnissen.
	var result int
	result = 1          // result bekommt den Wert 1
	result = result * 5 // result wird 5
	result = result * 4 // result wird 20
	result = result * 3 // result wird 60
	result = result * 2 // result wird 120
	result = result * 1 // result bleibt 120
	fmt.Println(result)

	// Berechnung mit Zwischenergebnissen, aber mit möglichst gleich bleibenden Anweisungen.

	// result zurücksetzen.
	result = 1
	var n int = 5

	result = result * n // result wird 5
	n = n - 1
	result = result * n // result wird 20
	n = n - 1
	result = result * n // result wird 60
	n = n - 1
	result = result * n // result wird 120
	n = n - 1
	result = result * n // result bleibt 120
	n = n - 1
	fmt.Println(result)

	// Das Gleiche in einer Schleife.
	result = 1
	n = 7
	for n != 0 {
		result = result * n
		n = n - 1
	}
	fmt.Println(result)

	// Ganz flexibel: Die Schleife in eine Funktion gepackt.
	fmt.Println(factorial(8))

}

func factorial(n int) int {
	//var result int = 1
	result := 1
	for n != 0 {
		result = result * n
		n = n - 1
	}
	return result
}
