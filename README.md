# Vorlesungsmaterial Informatik 1

## Infos/Literatur zum Programmieren

### [A Tour of Go](https://tour.golang.org)

Go-Einführung von der Go-Webseite.
Guter Überblick über die Sprache mit Browser-basierter Programmierumgebung zum selbst experimentieren.
Enthält einige Querverweise zu anderen Sprachen, sollte aber auch für Anfänger geeignet sein.

### [Go by Example](https://gobyexample.com)

Sammlung von Beispielen zu verschiedenen Themen/Aspekten der Programmiersprache Go.

### [Go-Tutorial bei golangbot.com](https://golangbot.com/golang-tutorial-part-1-introduction-and-installation/)

Tutorial zu Go mit Erklärung der Grundlagen.

### [Go-Tutorial von Gabriel Tanner](https://gabrieltanner.org/blog/an-introduction-to-golang)

Ein weiteres Tutorial zu Go mit Erklärung der Grundlagen.

### [CodinGame](https://www.codingame.com)

Online-Plattform, auf der Programmieraufgaben gelöst und KIs für kleinere Spiele geschrieben werden  sollen.
Die Aufgaben können im Browser in diversen Sprachen gelöst werden, u.A. auch Go.

### [ProjectEuler](https://projecteuler.net/)

Sammlung von mathematischen Problemen, die gelöst werden, indem man sich ein kleines Programm dafür schreibt.
Es ist keine Sprache oder Umgebung vorgegeben, man schreibt sich sein Programm auf dem eigenen PC und gibt online nur die Lösung ein.
Theoretisch sind die Aufgaben auch mit Stift und Papier lösbar, praktisch muss man programmieren, weil es von Hand zu viel Aufwand wäre.
