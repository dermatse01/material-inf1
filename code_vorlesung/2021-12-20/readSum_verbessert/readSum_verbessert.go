package main

import "fmt"

/* Aufgabe 1:
 *
 * Schreiben Sie eine Funktion, die den Benutzer immer wieder nach einer Zahl fragt,
 * bis er eine 0 eingibt. Anschließend soll die Summe der eingegebenen Zahlen
 * auf die Konsole ausgegeben werden.
 *
 * Hinweis: In der main()-Funktion steht ein Beispiel, wie man eine einzelne Zahl
 *          einliest und wieder ausgibt.
 */
func readSum() int {
	result := 0
	input := 1
	for input != 0 {
		fmt.Print("Bitte eine Zahl eingeben: ")
		fmt.Scanln(&input)
		result += input
	}
	return result
}

func main() {
	fmt.Println(readSum())
	fmt.Println(readSum())
}
