package main

/* Aufgabe 1:
 *
 * Schreiben Sie eine Funktion, die den Benutzer immer wieder nach einer Zahl fragt,
 * bis er eine 0 eingibt. Anschließend soll eine Liste zurückgegeben werden, die die
 * eingegebenen Zahlen enthält.
 *
 * Hinweis: Der Code aus der entsprechenden Aufgabe in `ein-ausgabe/zahlen_einlesen.go`
 *          kann wiederverwendet werden.
 */
func readNumbers() []int {
	result := make([]int, 0)
	// TODO
	return result
}

/* Aufgabe 2:
 *
 * Schreiben Sie die Funktion `readSum()` aus `ein-ausgabe/zahlen_einlesen.go` erneut.
 * Verwenden Sie dabei die obige Funktion `readNumbers()`.
 */
func readSum() int {
	// TODO
	return 0
}

/* Aufgabe 3:
 *
 * Schreiben Sie die Funktion `readFives()` aus `ein-ausgabe/zahlen_einlesen.go` erneut.
 * Verwenden Sie dabei die obige Funktion `readNumbers()`.
 */
func readFives() int {
	// TODO
	return 0
}

/* Aufgabe 4:
 *
 * Schreiben Sie eine Funktion `divisors()`.
 * Die Funktion soll eine Zahl `n` erwarten und eine Liste liefern, die alle Teiler
 * von `n` enthält.
 *
 * Beispiele:
 * - divisors(10) == {1,2,5,10}
 * - divisors(60) == {1,2,3,4,5,6,10,12,15,20,30,60}
 * - divisors(37) == {1,37}
 */
func divisors(n int) []int {
	result := make([]int, 0)
	// TODO
	return result
}

/* Aufgabe 4:
 *
 * Schreiben Sie (erneut) eine Funktion `isPrime()`.
 * Die Funktion soll eine Zahl `n` erwarten und true zurückliefern,
 * wenn n eine Primzahl ist.
 *
 * Nutzen Sie die Funktion `divisors()`
 */
func isPrime(n int) bool {
	// TODO
	return true
}

/* Aufgabe 5:
 *
 * Schreiben Sie eine Funktion `listMax()`, die eine Liste erwartet und die
 * sowohl Position als auch Wert des größten Elements bestimmt.
 *
 * Hinweis: Aufruf in main() z.B. mit `x,y := listMax(...)`
 */
func listMax(list []int) (int, int) {
	// TODO
	return 0, 0
}

func main() {
	// Testen Sie hier Ihre Funktionen.
}
