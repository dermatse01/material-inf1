// Aufgaben zu Funktionen, Schleifen und If-Then-Else
package main

import "fmt"

// Liefert das Doppelte von n.
func double(n int) int {
	// TODO: Hier das Ergebnis berechnen.
	return 0
}

// Tests für double().
func run_double() {
	fmt.Println(double(10)) // Erwarte: 20
	fmt.Println(double(2))  // Erwarte: 4
	fmt.Println(double(-5)) // Erwarte: -10
}

// Liefert die Summe der Zahlen von 1 bis n.
func sum(n int) int {
	// TODO: Hier das Ergebnis berechnen.
	return 0
}

// Tests für sum().
func run_sum() {
	fmt.Println(sum(10)) // Erwarte: 55
	fmt.Println(sum(3))  // Erwarte: 6
	fmt.Println(sum(9))  // Erwarte: 45
	fmt.Println(sum(0))  // Erwarte: 0
}

// Liefert die Summe aller Vielfachen von x, die kleiner als n sind.
func sumMultiples(x, n int) int {
	result := 0
	// TODO: Hier das Ergebnis berechnen.
	return result
}

// Tests für sumMultiples().
func run_sumMultiples() {
	fmt.Println(sumMultiples(3, 10)) // Erwarte: 18
	fmt.Println(sumMultiples(2, 10)) // Erwarte: 20
	fmt.Println(sumMultiples(10, 2)) // Erwarte: 0
}

// Liefert genau dann true, wenn n eine Primzahl ist.
func isPrime(n int) bool {
	// TODO: Das pauschale Return durch etwas sinnvolles ersetzen.
	return true
}

// Tests für isPrime().
func run_IsPrime() {
	fmt.Println(isPrime(1)) // Erwarte: false
	fmt.Println(isPrime(2)) // Erwarte:  true
	fmt.Println(isPrime(3)) // Erwarte:  true
	fmt.Println(isPrime(4)) // Erwarte:  false
	fmt.Println(isPrime(5)) // Erwarte:  true
}

// Liefert den größten Primfaktor der Zahl n.
func largestPrimeFactor(n int) int {
	// TODO: Hier das Ergebnis berechnen.
	return 0
}

// Tests für largestPrimeFactor().
func run_LargestPrimeFactor() {
	fmt.Println(largestPrimeFactor(10)) // Erwarte:  5
	fmt.Println(largestPrimeFactor(49)) // Erwarte:  7
	fmt.Println(largestPrimeFactor(60)) // Erwarte:  5
	fmt.Println(largestPrimeFactor(17)) // Erwarte:  17
	fmt.Println(largestPrimeFactor(1))  // Erwarte:  0
	fmt.Println(largestPrimeFactor(0))  // Erwarte:  0
}

// Liefert das kleinste gemeinsame Vielfache von m und n.
func lcm(m, n int) int {
	// TODO: Hier das Ergebnis berechnen.
	return 0
}

// Tests für lcm().
func run_Lcm() {
	fmt.Println(lcm(3, 5))   // Erwarte:   15
	fmt.Println(lcm(2, 5))   // Erwarte:   10
	fmt.Println(lcm(4, 10))  // Erwarte:   20
	fmt.Println(lcm(20, 5))  // Erwarte:   20
	fmt.Println(lcm(25, 10)) // Erwarte:   50
}

// Main-Funktion, ruft die Tests auf.
// Kommentieren Sie am Besten die Testfunktionen aus, die Sie gerade nicht benötigen.
func main() {
	run_double()
	run_sum()
	run_sumMultiples()
	run_IsPrime()
	run_LargestPrimeFactor()
	run_Lcm()
}
